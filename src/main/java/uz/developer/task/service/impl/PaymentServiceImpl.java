package uz.developer.task.service.impl;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import uz.developer.task.exception.DoesNotExistException;
import uz.developer.task.exception.ProductDoesNotExistException;
import uz.developer.task.exception.ValueException;
import uz.developer.task.models.custom.Response;
import uz.developer.task.models.custom.Status;
import uz.developer.task.models.dto.PaymentDto;
import uz.developer.task.models.entity.Invoice;
import uz.developer.task.models.entity.Payment;
import uz.developer.task.models.projection.PaymentDetailsProjection;
import uz.developer.task.repository.InvoiceRepository;
import uz.developer.task.repository.PaymentRepository;
import uz.developer.task.service.in.PaymentService;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PaymentServiceImpl implements PaymentService {
    private final PaymentRepository paymentRepository;
    private final InvoiceRepository invoiceRepository;

    @Override
    public Response paymentMake(PaymentDto paymentDto) {
        Response response = new Response();
        Status status;
        Optional<Invoice> optionalInvoice = invoiceRepository.findById(paymentDto.getInvoiceId());
        if (paymentDto.getAmount() <= 0) {
            throw new ValueException("this amount is not allowed");

        }
        if (!optionalInvoice.isPresent()) {
            throw new DoesNotExistException("invoise does not exist");

        } else {
            Payment payment = new Payment();
            payment.setAmount(paymentDto.getAmount());
            payment.setInvoice(optionalInvoice.get());
            payment.setTime(new Timestamp(new Date().getTime()));
            Payment savedPayment = paymentRepository.save(payment);

            status = new Status(0, "successfully");
            response.setStatus(status);
            response.setData(savedPayment.getId());
        }
        return response;
    }

    @Override
    public Response getDetailsByPaymentId(Long paymentId) {
        Response response = new Response();
        Status status;
        PaymentDetailsProjection paymentDetailsProjection = paymentRepository.getByPaymentId(paymentId);
        if (paymentDetailsProjection != null) {
            status = new Status(0, "success");
            response.setStatus(status);
            response.setData(paymentDetailsProjection);
        } else {
            throw new DoesNotExistException("not found");

        }
        return response;
    }
}
