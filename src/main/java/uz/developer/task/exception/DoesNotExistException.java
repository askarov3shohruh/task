package uz.developer.task.exception;

public class DoesNotExistException extends RuntimeException{
    public DoesNotExistException(String message) {
        super(message);
    }
}
