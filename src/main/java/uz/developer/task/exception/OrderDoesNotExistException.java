package uz.developer.task.exception;

public class OrderDoesNotExistException extends RuntimeException{
    public OrderDoesNotExistException(String message) {
        super(message);
    }
}
