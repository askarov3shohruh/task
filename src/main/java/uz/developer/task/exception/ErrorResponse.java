package uz.developer.task.exception;

import lombok.Value;
import org.springframework.lang.Nullable;

import java.util.Map;

@Value(staticConstructor = "of")
public class ErrorResponse {
    int code;
    String message;
    @Nullable
    Map<String, Object> meta;

    public static ErrorResponse of(int code, String msg) {
        return of(code, msg, null);
    }


    public static ErrorResponse of(ResponseCode responseCode) {
        return of(responseCode.getCode(), responseCode.getMessage());
    }

}
