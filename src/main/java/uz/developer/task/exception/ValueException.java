package uz.developer.task.exception;

public class ValueException extends RuntimeException{
    public ValueException(String message) {
        super(message);
    }
}
