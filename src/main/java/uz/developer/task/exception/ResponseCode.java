package uz.developer.task.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ResponseCode {

    NOT_FOUND(1091, "Object not found"),
    DATE_CONVERT(1090, "date convert exception"),
    CATEGORY_DOES_NOT(1092, "date convert exception"),
    PRODUCT_DOES_NOT(1093, "date convert exception"),
    VALUE_ERROR(1094, "this value exception"),
    GENERAL_ERROR(500, "General error");


    private final int code;
    private final String message;
}
