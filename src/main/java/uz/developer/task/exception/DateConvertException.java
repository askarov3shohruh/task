package uz.developer.task.exception;

public class DateConvertException extends RuntimeException{
    public DateConvertException(String message) {
        super(message);
    }
}
