package uz.developer.task.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
@Slf4j
public class ControllerAdvice {

    @ExceptionHandler({Exception.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorResponse on(Exception e) {
        log.error(ResponseCode.GENERAL_ERROR.getMessage(), e);
        return ErrorResponse.of(ResponseCode.GENERAL_ERROR.getCode(), ResponseCode.GENERAL_ERROR.getMessage() + " : " + e.getMessage());
    }

    @ExceptionHandler(DoesNotExistException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse on(DoesNotExistException e) {
        log.error(e.getMessage());
        return ErrorResponse.of(ResponseCode.NOT_FOUND);
    }
    @ExceptionHandler(DateConvertException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse on(DateConvertException e) {
        log.error(e.getMessage());
        return ErrorResponse.of(ResponseCode.DATE_CONVERT);
    }

    @ExceptionHandler(ValueException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorResponse on(ValueException e) {
        log.error(e.getMessage());
        return ErrorResponse.of(ResponseCode.VALUE_ERROR);
    }



}
